//
// Created by mnowak on 06.03.18.
//

#include <zconf.h>
#include <cstdlib>
#include <thread>
#include "Wall.h"
#include <ncurses.h>
#include <iostream>


Wall::Wall(int x, int y) {
    wall.resize(x);
    for (int i = 0; i < x; i++) {
        wall[i].resize(y, ' ');
    }
    filled.resize(y, x);
    for (int i = 0; i < y; i++) {
        mvprintw(i, 40, "%d - %d", i, filled[i]);
        refresh();
    }
}

void Wall::add_brick(int x, char c) {
    if (wall[0][x] == ' ') {
        mut.lock();
        wall[0][x] = c;
        draw();
        mut.unlock();
    }
}

bool Wall::is_filled() {
    for (int i = 0; i < filled.size(); i++) {
        if (filled[i] > 0)
            return false;
    }
    return true;
}

void Wall::move_brick(int x, int y) {
    bool flag = true;
    //sprawdza czy jest sens wchodzic w petle while
    //czy
    if (y == filled[x] - 1) {
        mut.lock();
        filled[x]--;
        mvprintw(x, 40, "%d - %d", x, filled[x]);
        refresh();
        mut.unlock();
        return;
    }
    while (flag) {
        if (can_move_down(x, y)) {
            mut.lock();
            wall[y + 1][x] = wall[y][x];
            wall[y][x] = ' ';
            y = y + 1;
            if (y == filled[x] - 1) {
                flag = false;
                filled[x]--;
            }

            mvprintw(x, 40, "%d - %d", x, filled[x]);
            draw();
            mut.unlock();
            usleep(move_speed);
        }
    }
}

bool Wall::can_move_down(int x, int y) {
    if ((y + 1 < wall.size()) && wall[y + 1][x] == ' ') {
        return true;
    }
    return false;
}

void Wall::run() {
    while (!is_filled()) {
        auto character = static_cast<char>(rand() % 26 + 'A');
        int col;
        do {
            col = static_cast<int>(rand() % wall[0].size());
        } while (filled[col] <= 0);
        add_brick(col, character);
        threads.push_back(thread(&Wall::move_brick, this, col, 0));
        usleep(creation_speed);
    }
}

void Wall::draw() {
    int x = wall.size();
    int y = wall[0].size();

    for (int i = 0; i < x; i++) {
        for (int j = 0; j < y; j++) {
            mvaddch(i + 1, j + 1, wall[i][j]);
        }
    }
    refresh();
}

void Wall::join_threads() {
    for (auto &t : threads) {
        t.join();
    }
}




