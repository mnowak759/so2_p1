#include <iostream>
#include "ncurses.h"
#include "Wall.h"

using namespace std;

void move_bricks(vector<vector<char>> wall) {
}

void draw_border(int size_x, int size_y) {
    for (int x = 0; x < size_x; x++) {
        for (int y = 0; y < size_y; y++) {
            if ((y == 0) || (y == size_y - 1) || (x == 0) || (x == size_x - 1)) {
                mvaddch(x, y, '#');
            }
        }
    }
}

int main() {
    int size_x = 10;
    int size_y = 30;

    initscr();
    auto *wall = new Wall(size_x - 2, size_y - 2);

    draw_border(size_x, size_y);
    refresh();
    wall->run();
    wall->join_threads();
    mvaddstr(0, 47, "Zakonczono");
    getch();
    endwin();
    return 0;
}