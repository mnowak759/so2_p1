//
// Created by mnowak on 06.03.18.
//
#include <vector>
#include <thread>

#ifndef SO2_P1_WALL_H
#define SO2_P1_WALL_H

#include <mutex>

using namespace std;


class Wall {
    vector<vector<char>> wall;
    __useconds_t move_speed = 50000;
    __useconds_t creation_speed = 50000;
    vector<thread> threads;
    vector<int> filled;
    mutex mut;

    void move_brick(int x, int y);

    void add_brick(int x, char c);

    bool is_filled();

    bool can_move_down(int x, int y);

    void draw();

public:
    Wall(int x, int y);

    void run();

    void join_threads();
};


#endif //SO2_P1_WALL_H
